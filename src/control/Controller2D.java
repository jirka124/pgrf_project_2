package control;

import crop.PolygonCropper;
import fill.*;
import model.Edge;
import model.Point;
import model.Polygon;
import rasterize.*;
import utils.ClosestFinder;
import view.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Controller2D implements Controller {
    public static enum ORIENTATION { CW, CCW };
    public static ORIENTATION orientation = ORIENTATION.CCW;
    public static enum EDIT_MODE { MOVE_CLOSEST, REMOVE_CLOSEST, INSERT_BETWEEN };
    public static boolean patternFillActive = true;
    public static PatternFill1 patternFill1 = new PatternFill1();
    public static PatternFill2 patternFill2 = new PatternFill2();

    private final Panel panel;
    private final GUI gui;

    private EDIT_MODE edit_mode_lm = EDIT_MODE.INSERT_BETWEEN;
    private EDIT_MODE edit_mode_rm = EDIT_MODE.MOVE_CLOSEST;
    private Polygon cuttingPoly;
    private Polygon croppedPoly;
    private PolygonCropper polygonCropper;
    private PolygonRasterizer polygonRasterizer;
    private LineRasterizerGraphics rasterizer;
    private Point underMovement_lm;
    private Point underMovement_rm;
    private Point[] closest_lm;
    private Point[] closest_rm;


    public Controller2D(Panel panel) {
        this.panel = panel;
        this.gui = new GUI(this);
        initObjects(panel.getRaster());
        initListeners(panel);

        this.update();
    }

    public void initObjects(Raster raster) {
        this.rasterizer = new LineRasterizerGraphics(raster);
        this.polygonRasterizer = new PolygonRasterizer(raster);
        this.underMovement_lm = null;
        this.underMovement_rm = null;
        this.closest_lm = new Point[2];
        this.closest_rm = new Point[2];
        this.polygonCropper = new PolygonCropper();
        this.cuttingPoly = new Polygon();
        this.croppedPoly = new Polygon();
        this.initDefaultCrop();
        this.initDefaultCutt();
    }
    private void initDefaultCrop() {
        this.croppedPoly.setColors(new Color(204, 153, 0), new Color(255, 242, 204), new Color(255, 255, 230));
    }
    private void initDefaultCutt() {
        this.cuttingPoly.setColors(new Color(0, 172, 230), new Color(204, 242, 255), new Color(230, 255, 255));

        this.cuttingPoly.addPoint(new Point(400, 500));
        this.cuttingPoly.addPoint(new Point(500, 400));
        this.cuttingPoly.addPoint(new Point(300, 400));
    }



    @Override
    public void initListeners(Panel panel) {
        panel.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isControlDown()) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        if(edit_mode_lm == EDIT_MODE.MOVE_CLOSEST) {
                            croppedPoly.setState(Polygon.states.PENDING);

                            Point p = ClosestFinder.findClosestPolygonPoint(croppedPoly, e.getX(), e.getY());
                            underMovement_lm = p;
                        }
                        else if(edit_mode_lm == EDIT_MODE.REMOVE_CLOSEST) {
                            Point p = ClosestFinder.findClosestPolygonPoint(croppedPoly, e.getX(), e.getY());
                            if (p != null) croppedPoly.delPoint(p);
                            update();
                        } else {
                            if (croppedPoly.size() < 2) return;
                            closest_lm = ClosestFinder.findClosestPolygonEdge(croppedPoly, e.getX(), e.getY());
                            int index1 = croppedPoly.indexOf(closest_lm[0]);
                            int index2 = croppedPoly.indexOf(closest_lm[1]);
                            int maxIndex = index2 == 0 ? 0 : Math.max(index1, index2);
                            Point newPoint = new Point(e.getX(), e.getY());
                            croppedPoly.addPoint(maxIndex, newPoint);
                            underMovement_lm = newPoint;
                        }
                    }
                    else if (SwingUtilities.isRightMouseButton(e)) {
                        if(edit_mode_rm == EDIT_MODE.MOVE_CLOSEST) {
                            cuttingPoly.setState(Polygon.states.PENDING);

                            Point p = ClosestFinder.findClosestPolygonPoint(cuttingPoly, e.getX(), e.getY());
                            underMovement_rm = p;
                        }
                        else if(edit_mode_rm == EDIT_MODE.REMOVE_CLOSEST) {
                            Point p = ClosestFinder.findClosestPolygonPoint(cuttingPoly, e.getX(), e.getY());
                            if (p != null) cuttingPoly.delPoint(p);
                            update();
                        } else {
                            if (cuttingPoly.size() < 2) return;
                            closest_rm = ClosestFinder.findClosestPolygonEdge(cuttingPoly, e.getX(), e.getY());
                            int index1 = cuttingPoly.indexOf(closest_rm[0]);
                            int index2 = cuttingPoly.indexOf(closest_rm[1]);
                            int maxIndex = index2 == 0 ? 0 : Math.max(index1, index2);
                            Point newPoint = new Point(e.getX(), e.getY());
                            cuttingPoly.addPoint(maxIndex, newPoint);
                            underMovement_rm = newPoint;
                        }
                    }

                } else {
                    if (e.isShiftDown()) {
                        //TODO
                    } else if (SwingUtilities.isLeftMouseButton(e)) {
                        croppedPoly.setState(Polygon.states.PENDING);

                        croppedPoly.addPoint(new Point(e.getX(), e.getY()));
                        update();
                    } else if (SwingUtilities.isRightMouseButton(e)) {
                        cuttingPoly.setState(Polygon.states.PENDING);

                        cuttingPoly.addPoint(new Point(e.getX(), e.getY()));
                        update();
                    } else if (SwingUtilities.isMiddleMouseButton(e)) {
                        Color fillColor = new Color(0, 204, 0);
                        Color backColor = new Color(panel.getRaster().getPixel(e.getX(), e.getY()));

                        SeedFill seedFill = new SeedFillBack(panel.getRaster(), new Point(e.getX(), e.getY()), backColor, fillColor);

                        update();
                        seedFill.fill();
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);

                if (SwingUtilities.isLeftMouseButton(e)) {
                    croppedPoly.setState(Polygon.states.COMPLETE);
                    update();
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    cuttingPoly.setState(Polygon.states.COMPLETE);

                    // assure outer polygon is convex, delete last point if not
                    if(!cuttingPoly.isConvex()) cuttingPoly.delLastPoint();
                    update();
                }
            }
        });

        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (e.isControlDown()) {
                    // process special edit: MOVE_CLOSEST
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        if (underMovement_lm != null) {
                            croppedPoly.setPoint(underMovement_lm, e.getX(), e.getY());
                            update();
                        }
                    }
                    else if (SwingUtilities.isRightMouseButton(e)) {
                        if (underMovement_rm != null) {
                            int holdX = underMovement_rm.x;
                            int holdY = underMovement_rm.y;

                            cuttingPoly.setPoint(underMovement_rm, e.getX(), e.getY());

                            if(!cuttingPoly.isConvex()) { // assure outer polygon is convex, return to previous if not
                                cuttingPoly.setPoint(underMovement_rm, holdX, holdY);
                            }
                            update();
                        }
                    }
                } else {
                    if (e.isShiftDown()) {
                        //TODO
                    } else if (SwingUtilities.isLeftMouseButton(e)) {
                        if(croppedPoly.size() == 1) croppedPoly.addPoint(new Point(e.getX(), e.getY()));
                        Point lastPoint = croppedPoly.getPoint(croppedPoly.size() - 1);
                        lastPoint.x = e.getX();
                        lastPoint.y = e.getY();
                    } else if (SwingUtilities.isRightMouseButton(e)) {
                        if(cuttingPoly.size() == 1) cuttingPoly.addPoint(new Point(e.getX(), e.getY()));
                        Point lastPoint = cuttingPoly.getPoint(cuttingPoly.size() - 1);
                        int holdX = lastPoint.x;
                        int holdY = lastPoint.y;

                        cuttingPoly.setPoint(lastPoint, e.getX(), e.getY());

                        if(!cuttingPoly.isConvex()) { // assure outer polygon is convex, return to previous if not
                            cuttingPoly.setPoint(lastPoint, holdX, holdY);
                        }
                    } else if (SwingUtilities.isMiddleMouseButton(e)) {
                        //TODO
                    }
                    update();
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if (e.isControlDown()) {
                    // update edit points for left and right mouse
                    closest_rm[0] = closest_rm[1] = closest_lm[0] = closest_lm[1] = null;

                    if(edit_mode_lm == EDIT_MODE.MOVE_CLOSEST) closest_lm[0] = ClosestFinder.findClosestPolygonPoint(croppedPoly, e.getX(), e.getY());
                    else if(edit_mode_lm == EDIT_MODE.REMOVE_CLOSEST) closest_lm[0] = ClosestFinder.findClosestPolygonPoint(croppedPoly, e.getX(), e.getY());
                    else closest_lm = ClosestFinder.findClosestPolygonEdge(croppedPoly, e.getX(), e.getY());

                    if(edit_mode_rm == EDIT_MODE.MOVE_CLOSEST) closest_rm[0] = ClosestFinder.findClosestPolygonPoint(cuttingPoly, e.getX(), e.getY());
                    else if(edit_mode_rm == EDIT_MODE.REMOVE_CLOSEST) closest_rm[0] = ClosestFinder.findClosestPolygonPoint(cuttingPoly, e.getX(), e.getY());
                    else closest_rm = ClosestFinder.findClosestPolygonEdge(cuttingPoly, e.getX(), e.getY());
                    update();
                }
            }
        });

        panel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                // Clean scene, toggle pattern fill and cycle between edit modes
                if (e.getKeyCode() == KeyEvent.VK_C) initObjects(panel.getRaster());
                else if (e.getKeyCode() == KeyEvent.VK_1 || e.getKeyCode() == KeyEvent.VK_NUMPAD1) edit_mode_lm = EDIT_MODE.MOVE_CLOSEST;
                else if (e.getKeyCode() == KeyEvent.VK_2 || e.getKeyCode() == KeyEvent.VK_NUMPAD2) edit_mode_lm = EDIT_MODE.REMOVE_CLOSEST;
                else if (e.getKeyCode() == KeyEvent.VK_3 || e.getKeyCode() == KeyEvent.VK_NUMPAD3) edit_mode_lm = EDIT_MODE.INSERT_BETWEEN;
                else if (e.getKeyCode() == KeyEvent.VK_4 || e.getKeyCode() == KeyEvent.VK_NUMPAD4) edit_mode_rm = EDIT_MODE.MOVE_CLOSEST;
                else if (e.getKeyCode() == KeyEvent.VK_5 || e.getKeyCode() == KeyEvent.VK_NUMPAD5) edit_mode_rm = EDIT_MODE.REMOVE_CLOSEST;
                else if (e.getKeyCode() == KeyEvent.VK_6 || e.getKeyCode() == KeyEvent.VK_NUMPAD6) edit_mode_rm = EDIT_MODE.INSERT_BETWEEN;
                else if (e.getKeyCode() == KeyEvent.VK_P) patternFillActive = !patternFillActive;
                update();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // Make edit help points disappear on CTRL released
                if (e.getKeyCode() == KeyEvent.VK_CONTROL) closest_rm[0] = closest_rm[1] = closest_lm[0] = closest_lm[1] = null;
                update();
            }
        });

        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                panel.resize();
                initObjects(panel.getRaster());
            }
        });
    }

    private void update() {
        panel.clear();
        this.polygonRasterizer.rasterize(this.cuttingPoly);

        this.polygonRasterizer.rasterize(this.croppedPoly);

        Polygon newPoly = this.polygonCropper.crop(croppedPoly, cuttingPoly);
        ScanLine scanLine3 = new ScanLine(this.panel.getRaster(), this.polygonRasterizer, newPoly);
        scanLine3.fill();

        drawHelpPoints(this.closest_lm, 0xffff00);
        drawHelpPoints(this.closest_rm, 0x00ffff);
        this.gui.drawGUI();
    }

    private void drawHelpPoints(Point[] points, int color) {
        for(int i = 0, size = 2; i < size; i++) {
            Point p = points[i];
            if(p == null) continue;

            for(int j = -5; j < 5; j++) {
                for(int m = -5; m < 5; m++) {
                    panel.getRaster().setPixel(j + p.x,m + p.y, color);
                }
            }
        }
    }

    private void hardClear() {
        panel.clear();
    }

    public Panel getPanel() {
        return this.panel;
    }
    public EDIT_MODE getEdit_mode_lm() {
        return edit_mode_lm;
    }

    public EDIT_MODE getEdit_mode_rm() {
        return edit_mode_rm;
    }
}
