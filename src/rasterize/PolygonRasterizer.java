package rasterize;

import model.Line;
import model.Point;
import model.Polygon;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PolygonRasterizer {

    protected LineRasterizerGraphics lineRasterizer;
    Raster raster;

    public PolygonRasterizer(Raster raster){
        this.raster = raster;
        lineRasterizer = new LineRasterizerGraphics(raster);
    }
    public void rasterize(Polygon polygon) {
        drawPolygon(polygon);
    }
    protected void drawPolygon(Polygon polygon) {
        int size = polygon.size();
        int iterator = polygon.size() - 2;
        if(size >= 2) {
            for(int i = 0; i < iterator; i++) {
                Line edge = new Line(polygon.getPoint(i), polygon.getPoint(i + 1), 0x895623);
                lineRasterizer.rasterize(edge, polygon.getCompleteColor());
            }
            if (polygon.getState() == Polygon.states.COMPLETE) {
                Line edge = new Line(polygon.getPoint(size - 2), polygon.getPoint(size - 1), 0x895623);
                lineRasterizer.rasterize(edge, polygon.getCompleteColor());
                edge = new Line(polygon.getPoint(size - 1), polygon.getPoint(0), 0x895623);
                lineRasterizer.rasterize(edge, polygon.getCompleteColor());
            } else {
                Line edge = new Line(polygon.getPoint(size - 2), polygon.getPoint(size - 1), 0xff0055);
                lineRasterizer.rasterize(edge, polygon.getPendingColor());
                edge = new Line(polygon.getPoint(size - 1), polygon.getPoint(0), 0xff0055);
                lineRasterizer.rasterize(edge, polygon.getPendingColor());
            }

        }
    }

}
