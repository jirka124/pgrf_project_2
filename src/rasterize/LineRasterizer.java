package rasterize;

import model.Line;

import java.awt.*;

public abstract class LineRasterizer {
    Raster raster;
    Color color;

    public LineRasterizer(Raster raster){
        this.raster = raster;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setColor(int color) {
        this.color = new Color(color);
    }

    public void rasterize(Line line, Color color) {
        this.rasterize((int)line.getX1(), (int)line.getY1(), (int)line.getX2(), (int)line.getY2(), color);
    }
    public void rasterize(Line line) {
        this.rasterize((int)line.getX1(), (int)line.getY1(), (int)line.getX2(), (int)line.getY2());
    }
    public void rasterize(int x1, int y1, int x2, int y2, Color color) {
        this.setColor(color);
        this.rasterize(x1, y1, x2, y2);
    }
    public void rasterize(int x1, int y1, int x2, int y2) {
        this.drawLine(x1, y1, x2, y2);
    }

    protected void drawLine(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && y1 == y2) raster.setPixel(x1, y1, this.color.getRGB());
        else {
            float k = (float)(y2 - y1) / (x2 - x1); // urči směrnici
            float q = y1 - k * x1; // urči posun x

            if (Math.abs(x1 - x2) > Math.abs(y1 - y2)) { // použij jako stupnici osu x
                int min = Math.min(x1, x2);
                int max = Math.max(x1, x2);

                for(int x = min, y; x <= max; x++) {
                    y = (int)(k * x + q);
                    raster.setPixel(x, y, this.color.getRGB());
                }
            } else { // použij jako stupnici osu y
                int min = Math.min(y1, y2);
                int max = Math.max(y1, y2);

                if(k == Double.POSITIVE_INFINITY || k == Double.NEGATIVE_INFINITY) // ošetři chybu svislé čáry
                    for(int y = min; y <= max; y++) raster.setPixel(x1, y, this.color.getRGB());
                else {
                    for(int y = min, x; y <= max; y++) {
                        x = (int)( (y - q) / k );
                        raster.setPixel(x, y, this.color.getRGB());
                    }
                }

            }
        }
    }
}
