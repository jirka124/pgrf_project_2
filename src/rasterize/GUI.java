package rasterize;

import control.Controller2D;

import java.awt.*;

public class GUI {
    private Controller2D controller2D;
    public GUI(Controller2D controller2D) {
        this.controller2D = controller2D;
    }

    public void drawGUI() {
        RasterBufferedImage raster = (RasterBufferedImage) controller2D.getPanel().getRaster();
        Graphics ctx = raster.getGraphics();
        ctx.drawString("Use LMB to edit cropped poly, Hold CTRL for special edit: " + controller2D.getEdit_mode_lm() + " [1-3]", 5, 15);
        ctx.drawString("Use RMB to edit cutting poly, Hold CTRL for special edit: " + controller2D.getEdit_mode_rm() + " [4-5]", 5, 30);
        ctx.drawString("Use MMB to seed fill area and C in order to reset scene. Pattern fill: " + Controller2D.patternFillActive + " [p]", 5, 45);
    }
}
