package fill;

import control.Controller2D;
import model.Point;
import rasterize.Raster;

import java.awt.*;
import java.util.LinkedList;
import java.util.Queue;

public abstract class SeedFill implements Filler {
    private Queue<Point> queue = new LinkedList<>();
    private Raster raster;
    private Point currSeed;
    protected int fillColor;

    public SeedFill(Raster raster, Point startPoint, Color fillColor) {
        this.raster = raster;
        this.queue.add(startPoint);
        this.fillColor = fillColor.getRGB();
    }

    @Override
    public void fill() {
        while((currSeed = queue.poll()) != null) this.seed();
    }
    private void seed() {
        int curSeedColor = this.raster.getPixel(currSeed.x, currSeed.y);
        if(this.shouldFill(curSeedColor)) {
            if (Controller2D.patternFillActive) {
                this.raster.setPixel(currSeed.x, currSeed.y, Controller2D.patternFill1.paint(currSeed.x, currSeed.y));
            } else {
                this.raster.setPixel(currSeed.x, currSeed.y, fillColor);
            }

            this.queue.add(new Point(currSeed.x + 1, currSeed.y));
            this.queue.add(new Point(currSeed.x - 1, currSeed.y));
            this.queue.add(new Point(currSeed.x, currSeed.y + 1));
            this.queue.add(new Point(currSeed.x, currSeed.y - 1));
        }
    }
    protected abstract boolean shouldFill(int curSeedColor);
}
