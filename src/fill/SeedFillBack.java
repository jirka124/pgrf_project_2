package fill;

import model.Point;
import rasterize.Raster;

import java.awt.*;

public class SeedFillBack extends SeedFill {
    private int backColor;

    public SeedFillBack(Raster raster, Point startPoint, Color backColor, Color fillColor) {
        super(raster, startPoint, fillColor);
        this.backColor = backColor.getRGB();
    }

    @Override
    protected boolean shouldFill(int curSeedColor) {
        return curSeedColor == this.backColor;
    }
}
