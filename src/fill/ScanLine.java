package fill;

import control.Controller2D;
import model.Edge;
import model.ScanLineEdge;
import model.Point;
import model.Polygon;
import rasterize.PolygonRasterizer;
import rasterize.Raster;
import utils.BubbleSort;

import java.util.ArrayList;
import java.util.Collections;

public class ScanLine implements Filler {
    //private final LineRasterizer lineRasterizer;
    private final Raster raster;
    private final PolygonRasterizer polygonRasterizer;
    private final Polygon polygon;

    public ScanLine(Raster raster, PolygonRasterizer polygonRasterizer, Polygon polygon) {
        this.raster = raster;
        this.polygonRasterizer = polygonRasterizer;
        this.polygon = polygon;
    }

    @Override
    public void fill() {
        scanLine();
    }

    private void scanLine() {
        if (this.polygon.size() < 3) {
            this.polygonRasterizer.rasterize(this.polygon);
        } else {
            ArrayList<ScanLineEdge> edges = new ArrayList<>();
            ArrayList<Integer> intersects = new ArrayList<>();

            for(int i = 0, size = this.polygon.size() - 1; i < size; i++) addEdge(edges, createEdge(i, i + 1));
            addEdge(edges, createEdge(this.polygon.size() - 1, 0));

            int yMin = Integer.MAX_VALUE;
            int yMax = Integer.MIN_VALUE;

            for(int i = 0, size = this.polygon.size(); i < size; i++) {
                Point p = this.polygon.getPoint(i);
                if(p.y < yMin) yMin = p.y;
                if(p.y > yMax) yMax = p.y;
            }

            for(int y = yMin; y <= yMax; y++) {
                intersects.clear();
                for(ScanLineEdge e: edges) if(e.isIntersection(y)) intersects.add(e.getIntersection(y));
                Collections.sort(intersects);
                new BubbleSort().sort(intersects);
                for(int i = 0, size = intersects.size(); i < size; i += 2)
                    for(int x = intersects.get(i); x < intersects.get(i + 1); x++) {
                        if (Controller2D.patternFillActive) {
                            this.raster.setPixel(x, y, Controller2D.patternFill2.paint(x, y));
                        } else {
                            this.raster.setPixel(x, y, this.polygon.getFillColor().getRGB());
                        }
                    }

                this.polygonRasterizer.rasterize(this.polygon);
            }
        }


    }

    private void addEdge(ArrayList<ScanLineEdge> edges, ScanLineEdge edge) {
        if(!edge.isHorizontal()) {
            edge.orientate();
            edge.calcDirectPush();
            edges.add(edge);
        }
    }

    private ScanLineEdge createEdge(int i1, int i2) {
        Point p1 = this.polygon.getPoint(i1);
        Point p2 = this.polygon.getPoint(i2);

        return new ScanLineEdge(p1.x, p1.y, p2.x, p2.y);
    }


}
