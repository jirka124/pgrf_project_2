package fill;


import model.Point;
import rasterize.Raster;

import java.awt.*;

public class SeedFillBorder extends SeedFill {
    private int borderColor;

    public SeedFillBorder(Raster raster, Point startPoint, Color borderColor, Color fillColor) {
        super(raster, startPoint, fillColor);
        this.borderColor = borderColor.getRGB();
    }

    @Override
    protected boolean shouldFill(int curSeedColor) {
        return curSeedColor != this.borderColor && curSeedColor != this.fillColor;
    }
}
