package fill;

public class PatternFill1 implements PatternFill {

    private int[][] pattern;
    private int sizeX;
    private int sizeY;

    public PatternFill1() {
        pattern = new int[6][6];
        for(int i = 0; i < 6; i++)
            for(int j = 0; j < 6; j++)
                pattern[i][j] = 0x585858;

        for(int i = 1; i < 5; i++) pattern[4][i] = 0xffffff;
        pattern[2][1] = pattern[1][1] = 0xffffff;
        pattern[1][4] = pattern[2][4] = 0xffffff;
        pattern[4][5] = pattern[4][0] = 0x585858;

        sizeY = pattern.length;
        sizeX = pattern[0].length;
    }

    @Override
    public int paint(int x, int y) {
        return pattern[y % sizeY][x % sizeX];
    }
}
