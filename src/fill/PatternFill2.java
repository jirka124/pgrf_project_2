package fill;

public class PatternFill2 implements PatternFill {

    private int[][] pattern;
    private int sizeX;
    private int sizeY;

    public PatternFill2() {
        pattern = new int[6][6];
        for(int i = 0; i < 6; i++)
            for(int j = 0; j < 6; j++)
                pattern[i][j] = 0xffffff;

        for(int i = 0; i < 6; i++) pattern[0][i] = 0x600080;
        for(int i = 0; i < 6; i++) pattern[5][i] = 0x600080;
        for(int i = 0; i < 6; i++) pattern[i][0] = 0x600080;
        for(int i = 0; i < 6; i++) pattern[i][5] = 0x600080;

        sizeY = pattern.length;
        sizeX = pattern[0].length;
    }

    @Override
    public int paint(int x, int y) {
        return pattern[y % sizeY][x % sizeX];
    }
}
