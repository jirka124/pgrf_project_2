package model;

import control.Controller2D;
import control.Controller2D.ORIENTATION;

import java.awt.*;
import java.util.ArrayList;

public class Polygon {
    public static enum states { COMPLETE, PENDING }
    private states state = states.COMPLETE;
    private final ArrayList<Point> points;
    private ArrayList<Edge> edges;
    private Point lastPoint;
    private Color completeColor;
    private Color pendingColor;
    private Color fillColor;

    public Polygon() {
        this(new ArrayList<>());
    }
    public Polygon(ArrayList<Point> points) {
        this.points = points;
        this.edges = new ArrayList<>();
        completeColor = new Color(255, 255, 255);
        pendingColor = new Color(255, 255, 255);
        fillColor = new Color(204, 204, 204);
    }

    public void setPoint(Point point, int newX, int newY) {
        if (this.points.indexOf(point) < 0) return;
        point.x = newX;
        point.y = newY;
        this.generateEdges();
    }
    public void addPoint(Point point) {
        this.points.add(point);
        this.lastPoint = point;
        this.generateEdges();
    }
    public void addPoint(int index, Point point) {
        this.points.add(index, point);
        this.lastPoint = point;
        this.generateEdges();
    }
    public void delPoint(int index) {
        this.points.remove(index);
        this.generateEdges();
    }
    public void delPoint(Point point) {
        this.points.remove(point);
        this.generateEdges();
    }
    public void clear() {
        this.points.clear();
        this.generateEdges();
    }
    public Point getPoint(int index) {
        return this.points.get(index);
    }
    public void delLastPoint() {
        this.points.remove(this.lastPoint);
        this.generateEdges();
    }
    public ArrayList<Point> getCopy() {
        return (ArrayList<Point>) this.points.clone();
    }
    public int size() {
        return this.points.size();
    }
    public int indexOf(Point point) {
        return this.points.indexOf(point);
    }

    private void generateEdges() {
        this.edges.clear();
        for(int i = 0, size = this.points.size(); i < size;) {
            Point p1 = this.points.get(i);
            Point p2 = this.points.get(++i % size);
            this.edges.add(new Edge(p1.x, p1.y, p2.x, p2.y));
        }
        this.orientate();
    }

    public void orientate() {
        double sum = 0;
        for (Edge edge: this.edges) sum += (edge.x2 - edge.x1) * (edge.y2 + edge.y1);
        ORIENTATION polyOri = sum > 0.0f ? ORIENTATION.CCW: ORIENTATION.CW;

        if(Controller2D.orientation != polyOri) for (Edge edge: this.edges) edge.switchCoords();
    }
    public boolean isConvex()
    {
        if (points.size() < 4) return true;

        boolean sign = false;

        for(int i = 0, size = points.size(); i < size; i++) {
            double dx1 = points.get((i + 2) % size).x - points.get((i + 1) % size).x;
            double dy1 = points.get((i + 2) % size).y - points.get((i + 1) % size).y;
            double dx2 = points.get(i).x - points.get((i + 1) % size).x;
            double dy2 = points.get(i).y - points.get((i + 1) % size).y;
            double zcrossproduct = dx1 * dy2 - dy1 * dx2;

            if (i == 0) sign = zcrossproduct > 0;
            else if (sign != zcrossproduct > 0) return false;
        }

        return true;
    }

    public void setColors(Color completeColor, Color pendingColor, Color fillColor) {
        this.completeColor = completeColor;
        this.pendingColor = pendingColor;
        this.fillColor = fillColor;
    }
    public states getState() {
        return state;
    }
    public void setState(states state) {
        this.state = state;
    }

    public Color getCompleteColor() {
        return completeColor;
    }
    public Color getPendingColor() {
        return pendingColor;
    }
    public Color getFillColor() { return fillColor; }
    public ArrayList<Edge> getEdges() {
        return this.edges;
    }
}
