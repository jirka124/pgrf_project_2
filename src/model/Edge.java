package model;

public class Edge {
    protected int x1, x2, y1, y2;

    public Edge(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    protected void switchCoords() {
        int holder = this.x1;
        this.x1 = this.x2;
        this.x2 = holder;

        holder = this.y1;
        this.y1 = this.y2;
        this.y2 = holder;
    }

    public int getX1() {
        return x1;
    }
    public int getX2() {
        return x2;
    }
    public int getY1() {
        return y1;
    }
    public int getY2() {
        return y2;
    }
}
