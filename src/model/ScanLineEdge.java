package model;

public class ScanLineEdge extends Edge {
    protected double k, q;

    public ScanLineEdge(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    public boolean isHorizontal() {
        return this.y1 == this.y2;
    }
    public void calcDirectPush() {
        k = (double)(this.y2 - this.y1) / (this.x2 - this.x1);
        q = this.y1 - this.k * this.x1;
    }
    public void orientate() {
        if (this.y1 > this.y2) this.switchCoords();
    }
    public boolean isIntersection(int y) {
        return y >= this.y1 && y < this.y2;
    }
    public int getIntersection(int y) {
        if(this.k == Double.POSITIVE_INFINITY || this.k == Double.NEGATIVE_INFINITY) // ošetři chybu svislé čáry
            return this.x1;
        return (int)( (y - this.q) / this.k );
    }
}
