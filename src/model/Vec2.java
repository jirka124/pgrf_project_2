package model;

public class Vec2 {
    public float dx, dy;
    public Vec2(float dx, float dy) {
        this.dx = dx;
        this.dy = dy;
    }
    public Vec2(Point p1, Point p2) {
        this.dx = p2.x - p1.x;
        this.dy = p2.y - p1.y;
    }
    public Vec2 getNormalVec1() {
        return new Vec2(-this.dy, this.dx);
    }
    public Vec2 getNormalVec2() {
        return new Vec2(this.dy, -this.dx);
    }
    public float getScalarProd(Vec2 vec) {
        return this.dx * vec.dx + this.dy * vec.dy;
    }
}
