package crop;

import model.Edge;
import model.Point;
import model.Polygon;
import model.Vec2;

import java.util.ArrayList;

public class PolygonCropper {
    public Polygon crop(Polygon croppedPolygon, Polygon cuttingPolygon) {
        ArrayList<Point> croppedPoints = croppedPolygon.getCopy();

        for(Edge e: cuttingPolygon.getEdges()) {
            ArrayList<Point> product = new ArrayList<>();
            Point e1 = new Point(e.getX1(), e.getY1());
            Point e2 = new Point(e.getX2(), e.getY2());

            for(int j = 0, size2 = croppedPoints.size(); j < size2; j++) {
                Point v1 = croppedPoints.get(j);
                Point v2 = croppedPoints.get((j + 1) % size2);
                if(this.isInside(v2, e1, e2)) {
                    if (!this.isInside(v1, e1, e2)) product.add(this.getIntersection(v1, v2, e1, e2));
                    product.add(new Point(v2.x, v2.y));
                } else {
                    if(this.isInside(v1, e1, e2)) product.add(this.getIntersection(v1, v2, e1, e2));
                }
            }
            croppedPoints = product;
        }

        return new Polygon(croppedPoints);
    }
    private boolean isInside(Point p, Point linePoint1, Point linePoint2) {
        Vec2 t = new Vec2(linePoint1, linePoint2);
        Vec2 n = t.getNormalVec1();
        Vec2 v = new Vec2(linePoint1, p);

        return n.getScalarProd(v) < 0.0f;
    }
    private Point getIntersection(Point p1, Point p2, Point e1, Point e2) {
        double px = ((p1.x * p2.y - p2.x * p1.y) * (e1.x - e2.x) - (e1.x * e2.y - e2.x * e1.y) * (p1.x - p2.x)) / (double)((p1.x - p2.x) * (e1.y - e2.y) - (p1.y - p2.y) * (e1.x - e2.x));
        double py = ((p1.x * p2.y - p2.x * p1.y) * (e1.y - e2.y) - (e1.x * e2.y - e2.x * e1.y) * (p1.y - p2.y)) / (double)((p1.x - p2.x) * (e1.y - e2.y) - (p1.y - p2.y) * (e1.x - e2.x));

        return new Point(px, py);
    }
}
