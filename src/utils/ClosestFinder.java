package utils;

import model.Point;
import model.Polygon;

public class ClosestFinder {
    public static double minDistance(Point a, Point b, Point e) {
        Point ab = new Point(b.x - a.x, b.y - a.y);
        Point be = new Point(e.x - b.x, e.y - b.y);
        Point ae = new Point(e.x - a.x, e.y - a.y);
        double ab_be = (ab.x * be.x + ab.y * be.y);
        double ab_ae = (ab.x * ae.x + ab.y * ae.y);
        double reqAns;
        if (ab_be > 0) {
            double y = e.y - b.y;
            double x = e.x - b.x;
            reqAns = Math.sqrt(x * x + y * y);
        }
        else if (ab_ae < 0) {
            double y = e.y - a.y;
            double x = e.x - a.x;
            reqAns = Math.sqrt(x * x + y * y);
        } else {
            double mod = Math.sqrt(ab.x * ab.x + ab.y + ab.y);
            reqAns = Math.abs(ab.x * ae.y - ab.y * ae.x) / mod;
        }
        return reqAns;
    }
    public static Point findClosestPolygonPoint(Polygon polygon, double clientX, double clientY) {
        Point closePoint = null;
        double closeDist = Double.POSITIVE_INFINITY;

        for (int i = 0, size = polygon.size(); i < size; i++) {
            Point point = polygon.getPoint(i);
            double dist = Math.sqrt(Math.pow(point.x - clientX, 2) + Math.pow(point.y - clientY, 2));
            if (dist < closeDist) {
                closePoint = point;
                closeDist = dist;
            }
        }

        return closePoint;
    }
    public static Point[] findClosestPolygonEdge(Polygon polygon, double clientX, double clientY) {
        Point findPoint = new Point(clientX, clientY);
        Point closePointA = null;
        Point closePointB = null;
        double closeDist = Double.POSITIVE_INFINITY;

        for (int i = 0, size = polygon.size(); i < size; i++) {
            Point point1 = polygon.getPoint(i);
            Point point2 = polygon.getPoint((i + 1) % size);
            double dist = minDistance(point1, point2, findPoint);
            if (dist < closeDist) {
                closePointA = point1;
                closePointB = point2;
                closeDist = dist;
            }
        }

        return new Point[] {closePointA, closePointB};
    }

}
